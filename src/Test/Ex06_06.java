package Test;

public class Ex06_06 {
	public int Plus(int v1, int v2) {
		int result;
		result = v1 + v2;
		return result;
	}
	public static void main(String[] args) {
		int hap;
		Ex06_06 p = new Ex06_06();
		hap = p.Plus(100, 200);
		System.out.printf("100과 200의 plus() 메소드 결과는 : %d\n", hap);
	}
}
