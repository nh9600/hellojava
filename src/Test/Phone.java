package Test;

import java.util.Scanner;

public class Phone {
	String name, tel;

	public Phone(String name, String tel) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getTel() {
		return tel;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String a1 = scanner.next();
		String a2 = scanner.next();
		Phone a = new Phone(a1, a2);
		Phone b = new Phone("a1", "a2");
		scanner.close();
	}

}
