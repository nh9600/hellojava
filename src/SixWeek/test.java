package SixWeek;

public class test {
	public static void main(String[] args) {
		int a = 10;
		int b = 10; 
		boolean c = (a <=b);
		System.out.println(c);
		
		//더블은 정확한 0.1, 플롯은 근삿값 0.1 = false 
		double d = 0.1;
		float e = 0.1f;
		boolean f = (d == e);
		System.out.println(f);
		
		//박하늘 주소를 저장 
		String test1 = "박하늘";
		String test2 = "박하늘";
		//새로운 박하늘 주소를 저장 
		String test3 = new String("신민철");
		
		System.out.println(test1 == test2);
		System.out.println(test3 == test2);
		System.out.println(test1.equals(test3));
	}

}
