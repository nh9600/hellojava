package AightWeek;

import java.util.Scanner;

public class Ex03_5 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int year;

		System.out.printf("출생연도를 입력하세요 : ");
		year = s.nextInt();

		if (year % 12 == 0) {
			System.out.printf("원숭이띠\n");
		} else if (year % 12 == 1) {
			System.out.printf("닭띠\n");
		} else if (year % 12 == 2) {
			System.out.printf("개띠\n");
		} else if (year % 12 == 3) {
			System.out.printf("돼지띠\n");
		} else {
			System.out.printf("양띠\n");
		}
		s.close();
	}
}
	