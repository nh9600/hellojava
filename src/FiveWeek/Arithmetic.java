package FiveWeek;

public class Arithmetic {
	
	public static void main(String[] args) {
		int a, b, add, diff, mult;
		double div;
		
		a = 5;
		b = 2;
		
		add = a+b;
		diff = a-b;
		mult = a * b;
		div = (double)a / (double)b;
		
		System.out.print("두 수의 덧셈:"+add+" 뺄셈:"+diff+" 곱셈:"+mult+" 나눗셈:"+ String.format("%.2f", div));
		
		
	}

}
