package ElevenWeek;

public class StringArray2 {
	public static void main(String[] args) {
		String[][] strArray = new String[3][];
		strArray[0] = new String[] { "java", "java" };
		strArray[1] = new String[2];
		strArray[2] = new String[] { "java", "c" };
		strArray[1][0] = new String("java");
		strArray[1][1] = "c";
		System.out.println(strArray[0][0] == strArray[0][1]);
		System.out.println(strArray[0][0] == strArray[1][0]);
		System.out.println(strArray[2][1] == strArray[1][1]);
		System.out.println(strArray[0][0].equals(strArray[2][1]));
	}

}
