package ElevenWeek;

public class ArrayCopy2 {
	public static void main(String[] args) {
		String[] oldStrArray = { "java", "array", "copy" };
		String[] newStrArray = new String[5];
		//oldStrArray의 0번째부터 복사, newStrArray의 0번째부터 넣기, oldStrArray의 길이만큼
		System.arraycopy(oldStrArray, 0, newStrArray, 0, oldStrArray.length);
		
		for (int i = 0; i < newStrArray.length; i++) {
			System.out.print(newStrArray[i] + ", ");
		}
	}

}
