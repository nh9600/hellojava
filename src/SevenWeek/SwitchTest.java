package SevenWeek;

public class SwitchTest {
	public static void main(String[] args) {
		int num = (int)(Math.random()*6)+1;
		char grade = 'A';
		
		switch (num) {
		case 1:
			System.out.println("안녕");
			break;

		default:
			System.out.println("안녕2");
			break;
		}
		
		switch (grade) {
		case 'A':
		case 'a':
			System.out.println("안녕하슈");
			break;

		default:
			System.out.println("안녕하슈2");
			break;
		}
	}

}
